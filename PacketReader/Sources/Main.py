import Newpacketreader as pr
import graphics as gr
import hpack
from os import listdir
from os.path import isfile, join


packets = []
My_decoder = hpack.Decoder()

files = [f for f in listdir("../packets/") if isfile(join("../packets", f))]



files.sort()
for f in files[0:] :
    print "str(f) "+str(f)
    packet = pr.packetreader("../packets/"+str(f), My_decoder)
    packet.name = str(f)
    packet.number = packet.name.split("_")[1]
    packets.append(packet)


graph = gr.graphics()

graph.displayMain(packets)
#middleFrame = Frame(root, height=root.winfo_height()/2, highlightthickness=2, highlightbackground="#000000")
#middleFrame.pack(side=TOP)
#for i in files[5:8] :
  #  print(i)
   # packetreader.packetreader(topFrame, i)
#packetreader.packetreader(middleFrame, "7")

