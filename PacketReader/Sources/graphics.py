# -*- coding: utf-8 -*-
"""
Created on Sun May 15 13:48:55 2016

@author: dorian
"""
from Tkinter import *
from random import choice

class graphics:
    
    def __init__(self):
        self.root = Tk()
        self.root.wm_title("Quic Packet Reader")
        self.root.geometry("800x600")
        self.root.update() 
        self.root.pack_propagate(False)
      
      
    def randomColor(self):
        hexChar = ['4','5','6','7','8','9','A','B','C','D','E','F']
        color = "#"
        for i in range(2):
            color+=choice(hexChar[4:])
        for i in range(4):
            color+=choice(hexChar)
        return color

        
    def displayMain(self, packets):
        color = "#C7E2EE"        
        mainCanvas = Canvas(self.root, bg=color )      
        myscrollbar=Scrollbar(self.root,orient="vertical",command=mainCanvas.yview)
        mainCanvas.configure(yscrollcommand=myscrollbar.set)
        myscrollbar.pack(side="right",fill="y")
        mainCanvas.pack(side="left")
        scrollFrame = Frame(mainCanvas)
        def myfunction(event):
            mainCanvas.configure(scrollregion=mainCanvas.bbox("all"),width=self.root.winfo_width(),height=self.root.winfo_height())
        mainCanvas.create_window((0,0),window=scrollFrame,anchor='nw')
        scrollFrame.bind("<Configure>",myfunction)
        for p in packets:
            onePacketFrame = Frame(scrollFrame)
            self.displayExchange(p, onePacketFrame)
            self.displayInfos(p, onePacketFrame)
            self.displayButton(p, onePacketFrame)
            onePacketFrame.pack(side = TOP)

            
        
        mainloop()
        
    def displayButton(self, packet, onePacketFrame):
        rightFrame = Frame(onePacketFrame, width=self.root.winfo_width()/3, highlightthickness=2, highlightbackground="#000000")
        rightFrame.pack(side=RIGHT)
        packetButton = Button(rightFrame, text=packet.name, command=lambda : self.displayPacket(packet) ,relief=FLAT, height=2)
        packetButton.pack(side=TOP)
    
    def displayExchange(self, packet, onePacketFrame):
        middleFrame = Frame(onePacketFrame, width=self.root.winfo_width()/3, highlightthickness=2, highlightbackground="#000000")
        middleFrame.pack(side=LEFT)
        if ("CtoS" in packet.name) :
            b = Label(middleFrame, text="Client ―――――――――▶ Server", bg="#ffffff", height = 2, width = 30,fg="#003399")
        else :
            if ("StoC" in packet.name) :
                b = Label(middleFrame, text="Client ◀――――――――― Server", bg="#ffffff", height = 2, width = 30, fg="#000033")        
        b.pack(side=TOP)

    def displayInfos(self, packet, onePacketFrame):
        leftFrame = Frame(onePacketFrame, width=self.root.winfo_width()/3, highlightthickness=2, highlightbackground="#000000")
        leftFrame.pack(side=LEFT)
        infos = "Packet number : "+packet.number+", Length : "+packet.length
        a = Label(leftFrame, text=infos, bg="#ffffff", height = 2, width = 30)
        a.pack(side=TOP)


    def displayPacket(self, packet):
        packetWindow = Tk()
        packetWindow.wm_title(packet.name)
        packetWindow.geometry("400x500")
        packetWindow.update()
        
        color = self.randomColor()
            
        onePacket=Frame(packetWindow, bg=color)
        onePacket.pack(side=LEFT)
        
        canvas=Canvas(onePacket, bg=color)
        frame=Frame(canvas, bg=color)
        myscrollbar=Scrollbar(onePacket,orient="vertical",command=canvas.yview)
        canvas.configure(yscrollcommand=myscrollbar.set)
        myscrollbar.pack(side="right",fill="y")
        canvas.pack(side="left")
        canvas.create_window((0,0),window=frame,anchor='nw')
        def myfunction(event):
            canvas.configure(scrollregion=canvas.bbox("all"),width=packetWindow.winfo_width(),height=packetWindow.winfo_height())
        frame.bind("<Configure>",myfunction)
        
        
        b = Label(frame, text=packet.length, bg=color)
        b.pack(side=TOP)
        
        b = Label(frame, text=packet.privateFlags, bg=color)
        b.pack(side=TOP)

        for f in packet.frames:
            b = Label(frame, text=f.number, bg=color)
            b.pack(side=TOP)
            b = Label(frame, text=f.type, bg=color)
            b.pack(side=TOP)
            b = Label(frame, text=f.header_infos, bg=color)
            b.pack(side=TOP)
            b = Label(frame, text=f.data, bg=color, wraplength=300)
            b.pack(side=TOP)
        
        mainloop()
    