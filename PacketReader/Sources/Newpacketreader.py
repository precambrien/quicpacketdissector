# *- coding: utf-8 -*-
import os, sys
import imp
import binascii
import copy
import fileinput
from math import *
import hpack
from Tkinter import *
import tkFont




class Packet:
    def __init__(self):
        self.name=""
        self.failed=False
        self.privateFlags = ""
        self.frames = []
        self.lenght = ""
    
class Frame:
    def __init__(self):
        self.number = ""
        self.type = ""
        self.header_infos = ""
        self.data = ""

#GLOBALS :
thisPacket = Packet()
thisFrame = Frame()
offsetValues = [0,16,24,32,40,48,56,64];
streamIDLenghtValues = [8,16,24,32];
largestObservedLenght=[8,16,32,48];
missingPacketsLength=[8,16,32,48];


#Some usefull function:


def hexToBinary(input):
    l=len(input)
    b = str(bin(int(input, 16)))[2:]
    nb=4*l - len(b)
    for x in range(0, nb):
        b = "0"+b
    return b
    
def readPieceOfMessage_toString(message,i,length):
    data = message[i:i+length]
    data = "".join(data)
    data = hex(int(data, 2))
    data = data.decode("hex")
    try:
        data = data.decode("utf-8")
        data = str(data)
    except UnicodeDecodeError:
        data = "unreadable peace of message"
    finally:
        return data
    
def readPieceOfMessage_toInt(message, i, length):
    if(len(message)-i-1-length<0):
        return -1
        sys.stderr.write("lenght lead to out of bound")
    #passage en little endian
    littleEnd = []
    for k in range(length/8):
        for j in range(8):
            littleEnd.append(message[i+length-(k+1)*8+j])
    
    resStr = "";
    for k in range(length):
        resStr = resStr+littleEnd[k]
    res = int(resStr,2)
    return res

#The packet's relative functions

def streamFrameHeader(message, i):
    global thisFrame
    i+=1
    thisFrame.header_infos += "Stream fin flag (half-close) : "+str(message[i])+"\n"
    # second flag : d
    i+=1;
    d = False;
    if(message[i] == "1"):
        thisFrame.header_infos += "Data length present, is 16bits"+"\n"
        d = True;
    else:
        thisFrame.header_infos += "Trame extends to the end of packet"+"\n"
    i+=1;
    # Bits 4, 5, 6 "ooo"
    offsetLengthValues = [0,16,24,32,40,48,56,64]; #define in rfc
    valStr = message[i]+message[i+1]+message[i+2];
    valInt = int(valStr, 2);
    thisFrame.header_infos += "Size of offset :"+str(offsetLengthValues[valInt])+" bits"+"\n"
    offsetLen = offsetLengthValues[valInt];
    i+=3;
    # Bits 7, 8 "ss"
    streamIDLenghtValues = [8,16,24,32]; #define in rfc
    valStr = message[i]+message[i+1];
    valInt = int(valStr, 2);
    thisFrame.header_infos += "Length of streamID :"+str(streamIDLenghtValues[valInt])+" bits "+"\n"
    sidLen = streamIDLenghtValues[valInt];
    i+=2;
    #On vient de passer le type-field, on lit donc l'id de la stream frame:
    sid = readPieceOfMessage_toInt(message, i, sidLen)
    thisFrame.header_infos += "StreamID :"+str(sid)+"\n"
    i+=sidLen
    if(offsetLen != 0):
        offset = readPieceOfMessage_toInt(message, i, offsetLen);
        thisFrame.header_infos += "Offset content :"+str(offset)+"\n"
    i+=offsetLen
    if (d):
        dataLen = readPieceOfMessage_toInt(message, i, 16)
        thisFrame.header_infos +=  "dataLen ="+str(dataLen)+"\n"
        i += 16
    else :
        dataLen = len(message)-i
    return dataLen,i
    #END OF STREAM FRAME HEADER
    
def streamFrameData(message, dataLen, i, My_decoder):
    global thisFrame
    data = message[i:i+dataLen+1]#copy the rest of message in data 
    data = "".join(data)
    data = hex(int(data, 2))
    data = list(data[2:-1])#to cut '0x' and 'L'
    data = "".join(data)

    
    try :
        data = data.decode("hex")
        data = data.encode('utf-8')
        thisFrame.data += "data :\n"+data+"\n"
        i+=dataLen
        return i
    except UnicodeDecodeError:
        thisFrame.data += "Data isn't utf-8"+"\n"
        try :
            data = My_decoder.decode(data, False)
            thisFrame.data += "TABLEAU :\t"
            for row in data:
                thisFrame.data += row[0] +" -> " +row [1]+"\n"
            #thisFrame.data += "\nEN BLOC : "+str(data)+"\n"
        except :
            thisFrame.data += "Can't un-hpack data"+"\n"
        finally:
            i+=dataLen
            return i
    except TypeError:
        thisFrame.data += "Empty data, probably an error when reading packet."+"\n"
        return -1

        
    
    

def ackFrame(message, i):
    global thisFrame
    i+=1
    nFlag = int(message[i])#On va sauveguarder le flag "n" pour savoir
    #si on doit s'attedre à trouver des champs "Number Ranges" dans la frame.
    if(message[i] == "1"):#Bit 3 "n"
        thisFrame.header_infos += "The frame has NACK ranges"+"\n"         
    else:
        thisFrame.header_infos += "The frame has'nt any NACK ranges"+"\n"
    i+=1
    if(message[i] == "1"):#Bit 4 "t"
        thisFrame.header_infos += "The Frame was cut (betwean at least two packets)"+"\n"
        #la tramme ne rentre pas dans 1 seul packet ou le nombre de ranges de NACK est trop grand (255)
    else:
        thisFrame.header_infos += "The whole frame is in this packet"+"\n"
    i+=1
    #Bit 5, 6 "ll"
    largestObservedLenght=[8,16,32,48];
    valStr = message[i]+message[i+1]
    valInt = int(valStr, 2)
    thisFrame.header_infos += "Length of the largest field oserved (ll) :"+str(largestObservedLenght[valInt])+" bits"+"\n"
    largestObsLen = largestObservedLenght[valInt];
    i+=2
    #Bit 7, 8 "mm"
    missingPacketsLength=[8,16,32,48];
    valStr = message[i]+message[i+1]
    valInt = int(valStr, 2)
    thisFrame.header_infos += "Lenght of missing packets (mm) :"+str(missingPacketsLength[valInt])+" bits"+"\n"
    missPacLen = missingPacketsLength[valInt]
    i+=2
    i+=8 #Pour passer les 8 bits de Recieved Entropy
    thisFrame.header_infos += "Largest Observed :"+str(readPieceOfMessage_toInt(message, i, largestObsLen))+"\n"
    i+=largestObsLen #Pour passer le champ "Largest Observed"
    i+=16 #Pour passer le Ack Delay
    numTimestamp = readPieceOfMessage_toInt(message, i, 8)
    i+=8 #Pour passer le Num Timestamp après l'avoir lu
    i+=8 #Pour passer le Delta Largest Observed
    i+=32 #Pour passer le First Timestamp
    for k in range(numTimestamp):
        i+=8 #Pour passer le Delta Largest Observed
        i+=16 #Pour passer Time Since Previous Timestamp
    numRanges = 0
    if(nFlag == "1"):
        numRanges = readPieceOfMessage_toInt(message, i, 8)
        i+=8 #Pour passer le Number Ranges après l'avoir lu
        for k in range(numRanges):
            i+=missPacLen;
            i+=8 #Pour passer le Range Length
    numRevived = 0;
    if(nFlag == "1"):
        numRevived = readPieceOfMessage_toInt(message, i, 8)
        i+=8 #Pour passer le Num Revived
        for k in range(numRevived):
            i+=largestObsLen #Pour passer le champ "Revived Packet Number", qui à la même taille que " Largest Observed"
    return i
    #END OF LA ACK FRAME

def stopWaitingFrame(message, i, seq_number):
    global thisFrame
    i+=1 #to exit the type field
    i+=8 #to pass the Sent Entropy field
    seq_number = "{0:b}".format(seq_number)
    seq_number_len = ceil(len(str(seq_number))/8)#calcul how many byte we need for the sequence number,
    #for that's the number of byte we expect for the 'Least unacked delta' field. (see RFC)
    seq_number_len = seq_number_len*8 #we need the number of bits
    thisFrame.header_infos += "stop waiting the packet n°"+str(readPieceOfMessage_toInt(message,i,seq_number_len))+"\n"
    i+=seq_number_len
    return i
    
def windowUpdateFrame(message, i):
    global thisFrame
    i+=1# to exit the type field
    StreamID = readPieceOfMessage_toInt(message, i , 32)
    if StramID == 0:
        thisFrame.header_infos += "Update connection-level flow control window"+"\n"
    else:
        thisFrame.header_infos += "Update stream n°"+str(StreamID)+"\n"
    i+=32
    thisFrame.header_infos +="New limit number of bytes that can be send in this srteam:"+str(readPieceOfMessage_toInt(message,i,64))+"\n"
    i+=64
    return i
        
def blockedFrame(message,i):
    global thisFrame
    i+=1# to exit the type field
    thisFrame.header_infos += "Block the stream n°"+str(readPieceOfMessage_toInt(message,i,32))+"\n"
    i+=32
    return i

def resetStreamFrame(message,i):
    global thisFrame
    i+=1# to exit the type field
    thisFrame.header_infos += "Stream being terminated: n°"+str(readPieceOfMessage_toInt(message,i,32))+"\n"
    i+=32
    thisFrame.header_infos += "Byte offset for the end of data ont this stream"+str(readPieceOfMessage_toInt(message,i,64))+"\n"
    i+=64
    thisFrame.header_infos += "QuicErrorCode :n°"+str(readPieceOfMessage_toInt(message,i,32))+"\n"
    i+=32
    return i
    
def connectionClosedFrame(message,i):
    global thisFrame
    i+=1# to exit the type field
    thisFrame.header_infos += "QuicErrorCode :n°"+str(readPieceOfMessage_toInt(message,i,32))+"\n"
    i+=32
    phraseLen = readPieceOfMessage_toInt(message,i,16)
    i+=16
    if phraseLen == 0:
        thisFrame.header_infos +=  "No user's message"+"\n"
    else:
        thisFrame.header_infos += "User's message:"+readPieceOfMessage_toString(message,i,phraseLen)+"\n"
    i+=phraseLen
    return i

def goAwayFrame(message,i):
    global thisFrame
    i+=1# to exit the type field
    thisFrame.header_infos += "QuicErrorCode :n°"+str(readPieceOfMessage_toInt(message,i,32))+"\n"
    i+=32
    thisFrame.header_infos += "Last good stream id:"+str(readPieceOfMessage_toInt(message,i,32))+"\n"
    i+=32
    phraseLen = readPieceOfMessage_toInt(message,i,16)
    i+=16
    if phraseLen == 0:
        thisFrame.header_infos +=  "No user's message"+"\n"
    else:
        thisFrame.header_infos += "User's message:"+readPieceOfMessage_toString(message,i,phraseLen)+"\n"
    i+=phraseLen
    return i
    
def privateHeader( message, i) :
    # private flags(8) + (optional) FEC (8)
    global thisPacket
    hasFec=0
    hasEntropy=0
    
    if(message[i] == "0"): 
        thisPacket.privateFlags += "PRIVATE FLAGS"+"\n"
        i+=5
        if(message[i] == "1"): #0000 0100
            thisPacket.privateFlags += "FEC packet"+"\n"
        else :
            thisPacket.privateFlags += "Data packet"+"\n"
        i+=1
        if(message[i] == "1"): #0000 0010
            thisPacket.privateFlags += "FEC header present"+"\n"
            hasFec=1
        else :
            thisPacket.privateFlags += "FEC header not present"+"\n"
        i+=1
        if(message[i] == "1"): #0000 0001
            thisPacket.privateFlags += "Entropy : yes"+"\n"
            hasEntropy=1
        else :
            thisPacket.privateFlags += "Entropy : no"+"\n"
        i+=1
    else :
        #header does not begin with 0
        thisPacket.privateFlags += "* Unusual header *"+"\n"
    if (hasFec==1) :
        fecHeader(message, i) 
        thisPacket.privateFlags += "FEC first number packet :"+"\n"
        i+=8
    return i, hasFec, hasEntropy


def packetreader(file_name, My_decoder):
    global thisPacket
    global thisFrame
    thisPacket = Packet()
    thisFrame = Frame()
    f = open(file_name);
    message = f.read().encode("hex")
    message = hexToBinary(message)
    message = list(message)

    thisPacket.length = str(len(message)/8)+" octet"
    i = 0
    compteur = 0

    i, hasFec, hasEntropy = privateHeader(message, i)   

    # follow : frame packet OR FEC packet
    # Only frame packet tested here as FEC disabled in latest QUIC versions
    
    while i < len(message):
        thisFrame = Frame()
        compteur+=1
        thisFrame.number += "Frame number : "+str(compteur)
        if(message[i] == "1"):
            thisFrame.type = "STREAM FRAME"
            dataLen,i = streamFrameHeader(message, i)
            i = streamFrameData(message, dataLen, i, My_decoder)
        else:
             i+=1
             if(message[i] == "1"):
                 thisFrame.type = "ACK FRAME"
                 i = ackFrame(message, i)
             elif(message[i] == "0"):
                 i+=1;
                 
                 if(message[i] == "1"):
                     thisFrame.type = "CONGESTION FEEDACK Frame (Not used yet)"
                     i=-1 #The Congestion Feedback Frame isn't used yet, we mark the error
                 else:
                     i+=2
                     if(message[i] == "1"):
                         i+=1;
                         if(message[i] == "1"):
                             i+=1;
                             if(message[i] == "1"):
                                 thisFrame.type = "PING Frame"
                                 i+=1
                             elif(message[i] == "0"):
                                 thisFrame.type = "STOP_WAITING Frame"
                                 i = stopWaitingFrame(i, message, file_name[0])#'file_name[0]' is why your packet files must begin by sequence number
                         elif(message[i] == "0"):
                             i+=1;
                             if(message[i] == "1"):
                                 thisFrame.type = "BLOCKED Frame"
                                 i = blockedFrame(message,i)
                             elif(message[i] == "0"):
                                 thisFrame.type = "WINDOW_UPDATE Frame"
                                 i = windowUpdateFrame(message,i)
                     elif(message[i] == "0"):
                         i+=1
                         if(message[i] == "1"):
                             i+=1;
                             if(message[i] == "1"):
                                 thisFrame.type = "GOAWAY Frame"
                                 i = goAwayFrame(message,i)
                             elif(message[i] == "0"):
                                 thisFrame.type = "CONNECTION_CLOSE Frame"
                                 i = connectionClosedFrame(message,i)
                         elif(message[i] == "0"):
                             i+=1;
                             if(message[i] == "1"):
                                 thisFrame.type = "RST_STREAM Frame"
                                 i = resetStreamFrame(message,i)
                             elif(message[i] == "0"):
                                 thisFrame.type = "PADDING Frame"
                                 i+=1

        print"Succesful ending of reading one frame"
        thisPacket.frames.append(copy.copy(thisFrame))
        if(i == len(message)):
            return copy.copy(thisPacket)
        if(i > len(message)):
            thisPacket.failed=True
            return copy.copy(thisPacket)
        if(i < 0):
            #an error occure when reading the packet, we choose to return it anyway          
            return copy.copy(thisPacket)


   
