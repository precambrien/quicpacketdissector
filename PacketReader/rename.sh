cd packets/
echo "Please enter the PID of first packet. For example, for packet 0_1_0142, enter 0142 :"
read first

echo "Now please enter PID of second packet. For example, for packet 1_1_1275 enter 1275 :"
read second


for f in *_$first; do
    a="$(echo $f | sed s/$premier/CtoS/)"
    mv "$f" "$a"
done

for f in *_$second; do
    a="$(echo $f | sed s/$second/StoC/)"
    mv "$f" "$a"
done

for f in [0-9]_*; do
    a="$(echo 0$f)"
    mv "$f" "$a"
done

echo "Done, bye."
